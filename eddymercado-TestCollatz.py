#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # new unit tests
    def test_read_2(self):
        s = "01 4000000000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 4000000000)

    def test_read_3(self):
        s = "10 01\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 1)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # new unit tests
    def test_eval_5(self):
        v = collatz_eval(500, 1)
        self.assertEqual(v, 144)

    def test_eval_6(self):
        v = collatz_eval(1500, 1000)
        self.assertEqual(v, 182)

    def test_eval_7(self):
        v = collatz_eval(2000, 2500)
        self.assertEqual(v, 209)

    def test_eval_8(self):
        v = collatz_eval(3000, 3500)
        self.assertEqual(v, 199)

    def test_eval_9(self):
        v = collatz_eval(4000, 4500)
        self.assertEqual(v, 215)

    def test_eval_10(self):
        v = collatz_eval(5000, 5500)
        self.assertEqual(v, 192)

    def test_eval_11(self):
        v = collatz_eval(6000, 6500)
        self.assertEqual(v, 262)

    def test_eval_12(self):
        v = collatz_eval(7000, 7500)
        self.assertEqual(v, 239)

    def test_eval_13(self):
        v = collatz_eval(8000, 8500)
        self.assertEqual(v, 234)

    def test_eval_14(self):
        v = collatz_eval(9000, 9500)
        self.assertEqual(v, 260)

    def test_eval_15(self):
        v = collatz_eval(10000, 10500)
        self.assertEqual(v, 255)

    def test_eval_16(self):
        v = collatz_eval(11000, 11500)
        self.assertEqual(v, 237)

    def test_eval_17(self):
        v = collatz_eval(12000, 12500)
        self.assertEqual(v, 263)

    def test_eval_18(self):
        v = collatz_eval(13000, 13500)
        self.assertEqual(v, 276)

    def test_eval_19(self):
        v = collatz_eval(14000, 14500)
        self.assertEqual(v, 209)

    def test_eval_20(self):
        v = collatz_eval(15000, 15500)
        self.assertEqual(v, 271)

    def test_eval_21(self):
        v = collatz_eval(16000, 16500)
        self.assertEqual(v, 266)

    def test_eval_22(self):
        v = collatz_eval(17000, 17500)
        self.assertEqual(v, 235)

    def test_eval_23(self):
        v = collatz_eval(18000, 18500)
        self.assertEqual(v, 248)

    def test_eval_24(self):
        v = collatz_eval(19000, 19500)
        self.assertEqual(v, 212)

    def test_eval_25(self):
        v = collatz_eval(20000, 20500)
        self.assertEqual(v, 243)

    def test_eval_26(self):
        v = collatz_eval(21000, 21500)
        self.assertEqual(v, 256)

    def test_eval_27(self):
        v = collatz_eval(22000, 22500)
        self.assertEqual(v, 269)

    def test_eval_28(self):
        v = collatz_eval(23000, 23500)
        self.assertEqual(v, 251)

    def test_eval_29(self):
        v = collatz_eval(24000, 24500)
        self.assertEqual(v, 251)

    def test_eval_30(self):
        v = collatz_eval(25000, 25500)
        self.assertEqual(v, 264)

    def test_eval_31(self):
        v = collatz_eval(26000, 26500)
        self.assertEqual(v, 277)

    def test_eval_32(self):
        v = collatz_eval(27000, 27500)
        self.assertEqual(v, 259)

    def test_eval_33(self):
        v = collatz_eval(28000, 28500)
        self.assertEqual(v, 259)

    def test_eval_34(self):
        v = collatz_eval(29000, 29500)
        self.assertEqual(v, 272)

    def test_eval_35(self):
        v = collatz_eval(30000, 30500)
        self.assertEqual(v, 272)

    def test_eval_36(self):
        v = collatz_eval(31000, 31500)
        self.assertEqual(v, 285)

    def test_eval_37(self):
        v = collatz_eval(32000, 32500)
        self.assertEqual(v, 267)

    def test_eval_38(self):
        v = collatz_eval(33000, 33500)
        self.assertEqual(v, 267)

    def test_eval_39(self):
        v = collatz_eval(34000, 34500)
        self.assertEqual(v, 311)

    def test_eval_40(self):
        v = collatz_eval(35000, 35500)
        self.assertEqual(v, 311)

    def test_eval_41(self):
        v = collatz_eval(36000, 36500)
        self.assertEqual(v, 249)

    def test_eval_42(self):
        v = collatz_eval(37000, 37500)
        self.assertEqual(v, 262)

    def test_eval_43(self):
        v = collatz_eval(38000, 38500)
        self.assertEqual(v, 244)

    def test_eval_44(self):
        v = collatz_eval(39000, 39500)
        self.assertEqual(v, 275)

    def test_eval_45(self):
        v = collatz_eval(40000, 40500)
        self.assertEqual(v, 275)

    def test_eval_46(self):
        v = collatz_eval(41000, 41500)
        self.assertEqual(v, 244)

    def test_eval_47(self):
        v = collatz_eval(42000, 42500)
        self.assertEqual(v, 288)

    def test_eval_48(self):
        v = collatz_eval(43000, 43500)
        self.assertEqual(v, 257)

    def test_eval_49(self):
        v = collatz_eval(44000, 44500)
        self.assertEqual(v, 270)

    def test_eval_50(self):
        v = collatz_eval(45000, 45500)
        self.assertEqual(v, 314)

    def test_eval_51(self):
        v = collatz_eval(46000, 46500)
        self.assertEqual(v, 283)

    def test_eval_52(self):
        v = collatz_eval(47000, 47500)
        self.assertEqual(v, 314)

    def test_eval_53(self):
        v = collatz_eval(48000, 48500)
        self.assertEqual(v, 252)

    def test_eval_54(self):
        v = collatz_eval(49000, 49500)
        self.assertEqual(v, 265)

    def test_eval_55(self):
        v = collatz_eval(50000, 99999)
        self.assertEqual(v, 351)

    def test_eval_56(self):
        v = collatz_eval(100000, 150000)
        self.assertEqual(v, 375)

    def test_eval_57(self):
        v = collatz_eval(200000, 250000)
        self.assertEqual(v, 443)

    def test_eval_58(self):
        v = collatz_eval(300000, 350000)
        self.assertEqual(v, 441)

    def test_eval_59(self):
        v = collatz_eval(400000, 450000)
        self.assertEqual(v, 449)

    def test_eval_60(self):
        v = collatz_eval(500000, 550000)
        self.assertEqual(v, 470)

    def test_eval_61(self):
        v = collatz_eval(600000, 650000)
        self.assertEqual(v, 509)

    def test_eval_62(self):
        v = collatz_eval(700000, 750000)
        self.assertEqual(v, 504)

    def test_eval_63(self):
        v = collatz_eval(800000, 850000)
        self.assertEqual(v, 525)

    def test_eval_64(self):
        v = collatz_eval(900000, 950000)
        self.assertEqual(v, 507)

    def test_eval_65(self):
        v = collatz_eval(123456, 173456)
        self.assertEqual(v, 383)

    def test_eval_66(self):
        v = collatz_eval(234567, 284567)
        self.assertEqual(v, 407)

    def test_eval_67(self):
        v = collatz_eval(345678, 395678)
        self.assertEqual(v, 441)

    def test_eval_68(self):
        v = collatz_eval(456789, 506789)
        self.assertEqual(v, 444)

    def test_eval_69(self):
        v = collatz_eval(567890, 617890)
        self.assertEqual(v, 447)

    def test_eval_70(self):
        v = collatz_eval(678901, 728901)
        self.assertEqual(v, 504)

    def test_eval_71(self):
        v = collatz_eval(789012, 839012)
        self.assertEqual(v, 525)

    def test_eval_72(self):
        v = collatz_eval(890123, 940123)
        self.assertEqual(v, 507)

    def test_eval_73(self):
        v = collatz_eval(123, 50123)
        self.assertEqual(v, 324)

    def test_eval_74(self):
        v = collatz_eval(456, 50456)
        self.assertEqual(v, 324)

    def test_eval_75(self):
        v = collatz_eval(789, 50789)
        self.assertEqual(v, 324)

    def test_eval_76(self):
        v = collatz_eval(1010, 52110)
        self.assertEqual(v, 324)

    def test_eval_77(self):
        v = collatz_eval(3030, 53330)
        self.assertEqual(v, 340)

    def test_eval_78(self):
        v = collatz_eval(5050, 55550)
        self.assertEqual(v, 340)

    def test_eval_79(self):
        v = collatz_eval(7070, 57770)
        self.assertEqual(v, 340)

    def test_eval_80(self):
        v = collatz_eval(9090, 59990)
        self.assertEqual(v, 340)

    def test_eval_81(self):
        v = collatz_eval(11111, 61111)
        self.assertEqual(v, 340)

    def test_eval_82(self):
        v = collatz_eval(33333, 78333)
        self.assertEqual(v, 351)

    def test_eval_83(self):
        v = collatz_eval(55555, 105555)
        self.assertEqual(v, 351)

    def test_eval_84(self):
        v = collatz_eval(77777, 127777)
        self.assertEqual(v, 354)

    def test_eval_85(self):
        v = collatz_eval(99999, 149999)
        self.assertEqual(v, 375)

    def test_eval_86(self):
        v = collatz_eval(222222, 272222)
        self.assertEqual(v, 443)

    def test_eval_87(self):
        v = collatz_eval(444444, 494444)
        self.assertEqual(v, 444)

    def test_eval_88(self):
        v = collatz_eval(666666, 716666)
        self.assertEqual(v, 504)

    def test_eval_89(self):
        v = collatz_eval(888888, 938888)
        self.assertEqual(v, 476)

    def test_eval_90(self):
        v = collatz_eval(100, 50100)
        self.assertEqual(v, 324)

    def test_eval_91(self):
        v = collatz_eval(300, 50300)
        self.assertEqual(v, 324)

    def test_eval_92(self):
        v = collatz_eval(500, 50500)
        self.assertEqual(v, 324)

    def test_eval_93(self):
        v = collatz_eval(700, 50700)
        self.assertEqual(v, 324)

    def test_eval_94(self):
        v = collatz_eval(900, 50900)
        self.assertEqual(v, 324)

    def test_eval_95(self):
        v = collatz_eval(1100, 51100)
        self.assertEqual(v, 324)

    def test_eval_96(self):
        v = collatz_eval(1300, 51300)
        self.assertEqual(v, 324)

    def test_eval_97(self):
        v = collatz_eval(1500, 51500)
        self.assertEqual(v, 324)

    def test_eval_98(self):
        v = collatz_eval(1700, 51700)
        self.assertEqual(v, 324)

    def test_eval_99(self):
        v = collatz_eval(1900, 51900)
        self.assertEqual(v, 324)

    def test_eval_100(self):
        v = collatz_eval(2100, 52100)
        self.assertEqual(v, 324)

    def test_eval_101(self):
        v = collatz_eval(2300, 52300)
        self.assertEqual(v, 324)

    def test_eval_102(self):
        v = collatz_eval(2500, 52500)
        self.assertEqual(v, 324)

    def test_eval_103(self):
        v = collatz_eval(2700, 52700)
        self.assertEqual(v, 340)

    def test_eval_104(self):
        v = collatz_eval(2900, 52900)
        self.assertEqual(v, 340)

    def test_eval_105(self):
        v = collatz_eval(3100, 53100)
        self.assertEqual(v, 340)

    def test_eval_106(self):
        v = collatz_eval(3300, 53300)
        self.assertEqual(v, 340)

    def test_eval_107(self):
        v = collatz_eval(3500, 53500)
        self.assertEqual(v, 340)

    def test_eval_108(self):
        v = collatz_eval(3700, 53700)
        self.assertEqual(v, 340)

    def test_eval_109(self):
        v = collatz_eval(3900, 53900)
        self.assertEqual(v, 340)

    def test_eval_110(self):
        v = collatz_eval(4100, 54100)
        self.assertEqual(v, 340)

    def test_eval_111(self):
        v = collatz_eval(4300, 54300)
        self.assertEqual(v, 340)

    def test_eval_112(self):
        v = collatz_eval(4500, 54500)
        self.assertEqual(v, 340)

    def test_eval_113(self):
        v = collatz_eval(4700, 54700)
        self.assertEqual(v, 340)

    def test_eval_114(self):
        v = collatz_eval(4900, 54900)
        self.assertEqual(v, 340)

    def test_eval_115(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_116(self):
        v = collatz_eval(500000, 500001)
        self.assertEqual(v, 152)

    def test_eval_117(self):
        v = collatz_eval(999000, 999500)
        self.assertEqual(v, 396)

    def test_eval_118(self):
        v = collatz_eval(100000, 100100)
        self.assertEqual(v, 310)

    def test_eval_119(self):
        v = collatz_eval(300000, 300500)
        self.assertEqual(v, 371)

    def test_eval_120(self):
        v = collatz_eval(50000, 50500)
        self.assertEqual(v, 265)

    def test_eval_121(self):
        v = collatz_eval(75000, 75500)
        self.assertEqual(v, 307)

    def test_eval_122(self):
        v = collatz_eval(900000, 900500)
        self.assertEqual(v, 370)

    def test_eval_123(self):
        v = collatz_eval(150000, 150500)
        self.assertEqual(v, 370)

    def test_eval_124(self):
        v = collatz_eval(250000, 250500)
        self.assertEqual(v, 332)

    def test_eval_125(self):
        v = collatz_eval(400000, 400500)
        self.assertEqual(v, 374)

    def test_eval_126(self):
        v = collatz_eval(123456, 123956)
        self.assertEqual(v, 305)

    def test_eval_127(self):
        v = collatz_eval(234567, 235067)
        self.assertEqual(v, 337)

    def test_eval_128(self):
        v = collatz_eval(345678, 346178)
        self.assertEqual(v, 441)

    def test_eval_129(self):
        v = collatz_eval(456789, 457289)
        self.assertEqual(v, 356)

    def test_eval_130(self):
        v = collatz_eval(567890, 568390)
        self.assertEqual(v, 315)

    def test_eval_131(self):
        v = collatz_eval(678901, 679401)
        self.assertEqual(v, 292)

    def test_eval_132(self):
        v = collatz_eval(789012, 789512)
        self.assertEqual(v, 406)

    def test_eval_133(self):
        v = collatz_eval(890123, 890623)
        self.assertEqual(v, 339)

    def test_eval_134(self):
        v = collatz_eval(990000, 990500)
        self.assertEqual(v, 321)

    def test_eval_135(self):
        v = collatz_eval(800000, 800500)
        self.assertEqual(v, 375)

    def test_eval_136(self):
        v = collatz_eval(600000, 600500)
        self.assertEqual(v, 372)

    def test_eval_137(self):
        v = collatz_eval(700000, 700500)
        self.assertEqual(v, 367)

    def test_eval_138(self):
        v = collatz_eval(200000, 200500)
        self.assertEqual(v, 342)

    def test_eval_139(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_140(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)

    def test_eval_141(self):
        v = collatz_eval(900000, 999999)
        self.assertEqual(v, 507)

    def test_eval_142(self):
        v = collatz_eval(1, 10000)
        self.assertEqual(v, 262)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # new unit tests
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1, 10000, 262)
        self.assertEqual(w.getvalue(), "1 10000 262\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 900000, 999999, 507)
        self.assertEqual(w.getvalue(), "900000 999999 507\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # new unit test
    def test_solve_2(self):
        r = StringIO("2100 52100\n2300 52300\n2500 52500")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2100 52100 324\n2300 52300 324\n2500 52500 324\n")

    def test_solve_3(self):
        r = StringIO("999999 999999\n900000 999999\n1 10000")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999999 999999 259\n900000 999999 507\n1 10000 262\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
