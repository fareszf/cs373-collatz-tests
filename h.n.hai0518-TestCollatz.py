#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)
    
    def test_read_2(self):
        s = "8 16\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 8)
        self.assertEqual(j, 16)
    
    def test_read_3(self):
        s = "100 150\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 150)
    
    def test_read_4(self):
        s = "2 131072\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 2)
        self.assertEqual(j, 131072)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)
        # self.assertEqual(v, 1)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)
        # self.assertEqual(v, 1)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)
        # self.assertEqual(v, 1)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        # self.assertEqual(v, 1)

    def test_eval_5(self):
        v = collatz_eval(1, 1000)
        self.assertEqual(v, 179)

    def test_eval_6(self):
        v = collatz_eval(20, 10)
        self.assertEqual(v, 21)

    def test_eval_7(self):
        v = collatz_eval(1, 100)
        self.assertEqual(v, 119)

    # -----
    # print
    # -----
    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
    
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 70, 50, 113)
        self.assertEqual(w.getvalue(), "70 50 113\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1024, 2048, 182)
        self.assertEqual(w.getvalue(), "1024 2048 182\n")

    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n1 1\n20 10\n1 100\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n1 1 1\n20 10 21\n1 100 119\n")
    
    def test_solve_2(self):
        r = StringIO("100 50\n10001 10002\n10 11\n999 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "100 50 119\n10001 10002 180\n10 11 15\n999 1000 112\n"
        )
    
    def test_solve_3(self):
        r = StringIO("2 4\n128 256\n256 512\n65536 131072\n131072 262144\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "2 4 8\n128 256 128\n256 512 144\n65536 131072 354\n131072 262144 443\n"
        )

    def test_solve_4(self):
        r = StringIO("2 131072\n65535 65536\n10 999999\n65536 262144\n4096 16384\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "2 131072 354\n65535 65536 131\n10 999999 525\n65536 262144 443\n4096 16384 276\n"
        )

# ----
# main
# ----
if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
